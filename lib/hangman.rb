class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  def setup
    secret_length = @referee.pick_secret_word
    @guesser.register_secret_length(secret_length)
    @board = Array.new(secret_length)
  end

  def update_board(char, indexes)
    indexes.each do |i|
      @board[i] = char
    end
  end

  def take_turn
    guess = @guesser.guess
    indexes = @referee.check_guess(guess)
    unless indexes.empty?
      self.update_board(guess, indexes)
    end
    @guesser.handle_response
  end

end

class HumanPlayer
end

class ComputerPlayer
  attr_reader :dictionary, :candidate_words
  def initialize(dictionary)
    @dictionary = dictionary
    @candidate_words = dictionary
  end

  def register_secret_length(length)
    @secret_length = length
    @candidate_words.select! { |x| x.length == length }
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def check_guess(guess_char)
    @secret_word.chars.each_index.select { |i| @secret_word[i] == guess_char }
  end

  def handle_response(char, indexes)
    if indexes.empty?
      @candidate_words.reject! { |word| word.include?(char) }
    else
      @candidate_words.select! do |word|
        indexes_in_word = (0..word.length).select { |i| word[i] == char }
        indexes_in_word == indexes
      end
    end
  end

  def guess(board)
    jumble = @candidate_words.join
    hash = {}
    jumble.chars.uniq.each do |char|
      unless board.include?(char)
        hash[char] = jumble.count(char)
      end
    end
    hash.keys.select { |key| hash[key] == hash.values.max }[0]
  end
end
